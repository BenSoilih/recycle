# -*-coding: utf-8 -*

import pickle
import random

from tkinter import *
from tkinter import ttk

class Interface():
    
    def __init__(self, ws, title, left_frame, right_frame):
        self.ws = ws 
        self.ws.title(title)
        self.ws.geometry('940x600')
        self.ws.config(bg='#4b8b3b')
        self.left_frame = left_frame
        self.right_frame = right_frame
        
    
    def creer_label_haut(self,title):
        Label(self.left_frame, text=title, font=('Times', 16)).grid(row=0, column=0, sticky=W, pady=10)
 
    def creer_label_text_bas(self,labels):
        i = 0
        vals = {}
        for ind in range(len(labels)):
           vals[("var" + str(ind))] = None 
        for key in vals.keys() :
            Label(self.right_frame, text=labels[i], font=('Times', 14)).grid(row=i, column=0, sticky=W, pady=10)
            key = Entry(self.right_frame, font=('Times', 14))
            key.grid(row=i, column=1, pady=10, padx=20)
            i = i + 1
            
    def creer_label_text_histo(self,labels,commandes):
        for i in range(len(labels)):
            Label(self.right_frame, text=labels[i], font=('Times', 14)).grid(row=i, column=0, sticky=W, pady=10)
            Label(self.right_frame, text=commandes[i], font=('Times', 14)).grid(row=i, column=1, sticky=W, pady=10)
            i = i + 1
    
    def creer_btn_text_bas(self,btn,labels,met):
        i = len(labels) + 1
        j = 0
        vals = {}
        for ind in range(len(btn)):
           vals[("var" + str(ind))] = None 
        for key in vals.keys() :
            key = Button(self.right_frame, width=15, text=btn[j], font=('Times', 14), command=met[j])
            key.grid(row=i, column=1, pady=10, padx=20)
            i = i + 1
            j = j + 1

    def placer(self,tup):
        (lfx1,lfy1,lfx2,lfy2) = tup
        self.left_frame.place(x=lfx1, y=lfy1)
        self.right_frame.place(x=lfx2, y=lfy2)
        self.ws.mainloop()


class Gestion():

    def id_choose(self, dic):
        l = []
        for keys in dic.keys():
           l.append(keys)
        i = random.randrange(2000)
        while(i in l):
            i = random.randrange(2000)
        return i

    def lire(self, path):
        with open(path, 'rb') as fichier:
            mon_depickler = pickle.Unpickler(fichier)
            dic = mon_depickler.load()
        return dic

    def ecrire(self, path, dic):
        with open(path, 'wb') as fichier:
            mon_pickler = pickle.Pickler(fichier)
            mon_pickler.dump(dic)
    
    def litres_totaux(self, path):
        ret = 0
        with open(path, 'rb') as fichier:
            mon_depickler = pickle.Unpickler(fichier)
            dic = mon_depickler.load()
        for valeur in dic.keys():        
            ret = ret + valeur.litres
        return ret
    
    def commandes_lire(self):
        mon_fichier = open("data/commandes.txt", "r")
        contenu = mon_fichier.read()
        return int(contenu)

    def commandes_ecrire(self, c):
         mon_fichier = open("data/commandes.txt", "w") 
         mon_fichier.write(str(c))
 
class Fournisseur(Gestion):

    def __init__(self, nom, litres, prix):
        self.nom = nom
        self.litres = litres
        self.prix = prix 

    def listeF(self):
        dic = self.lire('data/fournisseurs')
        return dic

    def donner(self, nom, litres, prix):
        dic = self.listeF()
        id = self.id_choose(dic)
        p = int(prix) * int(litres)
        d = {"id" : id,
             "nom" : nom,
             "litres": int(litres),
             "prix": p
        }
        dic[id] = d
        self.ecrire('data/fournisseurs', dic)
        com = self.commandes_lire()
        com = com + int(litres)
        self.commandes_ecrire(com)
    
    
class Client(Gestion):

    def __init__(self, nom, prenom, adresse, litres):
        self.nom = nom
        self.prenom = prenom
        self.adresse = adresse
        self.litres = litres 

    def listeC(self):
        dic = self.lire('data/clients')
        return dic

    def commander(self, nom, prenom, adresse, litres):
        curt = self.commandes_lire()      
        curt = curt - int(litres)
        dic = self.listeC()
        id = self.id_choose(dic)
        d = {"id" : id,
            "nom" : nom,
            "prenom" : prenom,
            "adresse" : adresse,
            "litres": int(litres)
        }
        dic[id] = d
        self.ecrire('data/clients', dic)
        self.commandes_ecrire(curt) 

class Main(Interface):
    def __init__(self, ws, title, left_frame, right_frame):
        super().__init__(ws, title, left_frame, right_frame)
        self.creer(ws, title, left_frame, right_frame)  
    
    def get_the_data(self):
        l = []
        children_widgets = self.right_frame.winfo_children()
        for child_widget in children_widgets:
            if child_widget.winfo_class() == 'Entry':
                l.append(child_widget.get())
        return l
        
    def fourni_val(self):
        data = self.get_the_data()
        f = Fournisseur(data[0],data[1],data[2])
        f.donner(data[0],data[1],data[2])
        
    def fourni_histo(self):
        data = self.get_the_data()
        f = Fournisseur(data[0],data[1],data[2])
        his = f.listeF()
        noms = []
        litres = []
        for v in his.values():
            noms.append(v['nom'])
            litres.append(str(v['litres']) + " litre(s)")
        self.ws.destroy()
        self.ws = Tk()
        self.left_frame = Frame(self.ws, bd=2, relief=SOLID, padx=10, pady=10)
        self.right_frame = Frame(self.ws, bd=2, relief=SOLID, padx=10, pady=10)
        objet1 = Interface(self.ws,"Recyclage des huiles",self.left_frame,self.right_frame)
        objet1.creer_label_haut("Historique des huiles Fournies.")
        objet1.creer_label_text_histo(noms,litres)
        objet1.placer((310,20,310,140))

    
    def constFourni(self):
        self.ws = Tk()
        self.left_frame = Frame(self.ws, bd=2, relief=SOLID, padx=10, pady=10)
        self.right_frame = Frame(self.ws, bd=2, relief=SOLID, padx=10, pady=10)
        labels = ["Votre nom", "nb de litres", "le prix du litre\n(0 si gratuit)"]
        btn = ['Valider', 'Historique']
        met = [self.fourni_val,self.fourni_histo]
        objet1 = Interface(self.ws,"Recyclage des huiles",self.left_frame,self.right_frame)
        objet1.creer_label_haut("Recycler vos huiles usagées avec nous.\nEnsemble, protégeons la nature!!")
        objet1.creer_label_text_bas(labels)
        objet1.creer_btn_text_bas(btn,labels,met)
        objet1.placer((310,20,310,140))
    
    def cli_val(self):
        data = self.get_the_data()
        c = Client(data[0],data[1],data[2],data[3])
        c.commander(data[0],data[1],data[2],data[3])
        
    def cli_histo(self):
        data = self.get_the_data()
        c = Client(data[0],data[1],data[2],data[3])
        his = c.listeC()
        noms = []
        litres = []
        for v in his.values():
            noms.append(str(v['nom']) + " " + str(v['prenom']))
            litres.append(str(v['litres']) + " litre(s)")
        self.ws.destroy()
        self.ws = Tk()
        self.left_frame = Frame(self.ws, bd=2, relief=SOLID, padx=10, pady=10)
        self.right_frame = Frame(self.ws, bd=2, relief=SOLID, padx=10, pady=10)
        objet1 = Interface(self.ws,"Recyclage des huiles",self.left_frame,self.right_frame)
        objet1.creer_label_haut("Historique des ventes effectuées.")
        objet1.creer_label_text_histo(noms,litres)
        objet1.placer((310,20,310,140))   
       
    def constClient(self):
        self.ws = Tk()
        self.left_frame = Frame(self.ws, bd=2, relief=SOLID, padx=10, pady=10)
        self.right_frame = Frame(self.ws, bd=2, relief=SOLID, padx=10, pady=10)
        labels = ["Votre nom","Votre prénom", "adresse", "nombre de litres"]
        btn = ['Valider', 'Historique']
        met = [self.cli_val,self.cli_histo]
        g = Gestion()
        objet1 = Interface(self.ws,"Recyclage des huiles",self.left_frame,self.right_frame)    
        objet1.creer_label_haut("Acheter votre biodiesél avec nous.\n" + str(g.commandes_lire()) + " litres disponibles.")
        objet1.creer_label_text_bas(labels)
        objet1.creer_btn_text_bas(btn,labels,met)
        objet1.placer((310,20,310,140))
    
    def client(self):
        self.ws.destroy()
        self.constClient()  
    
    def fournisseur(self):
        self.ws.destroy()
        self.constFourni()  
    
    def creer(self, ws, title, left_frame, right_frame):
        objetMain = Interface(ws,title,left_frame, right_frame)
        objetMain.creer_label_haut("Fournissez-nous de l'huile usagée.\nOu acheter votre biodiesél chez nous.")
        btn = ["Fournisseur", "Client"]
        var1 = Button(self.right_frame, width=15, text=btn[0], font=('Times', 14), command=self.fournisseur)
        var1.grid(row=0, column=1, pady=10, padx=20)
        var2 = Button(self.right_frame, width=15, text=btn[1], font=('Times', 14), command=self.client)
        var2.grid(row=1, column=1, pady=10, padx=20)
        objetMain.placer((310,20,360,140))

m = Tk()
left = Frame(m, bd=2, relief=SOLID, padx=10, pady=10)
right = Frame(m, bd=2, relief=SOLID, padx=10, pady=10)

l = Main(m,"Bienvenu(e)",left,right)        
